### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ ecd4cd68-536a-11eb-05a1-03f742d58318
using LinearAlgebra, SymEngine, RowEchelon

# ╔═╡ a8af804a-536a-11eb-0659-63c5920aafa2
md"# Pluto Quick-start
For use in SF1672, and similar courses, that rely a lot on linear algebra.

The first thing you might ask, is how to write text or equations. The easiest way to accomplish that is to make a new cell and use the `md\"MARKDOWN\"` syntax, where MARKDOWN is replaced with your text in markdown. In this mode, MathJax is supported, so by wrapping your equation in \"\$\", you can write LaTeX. Typically you will then hide the generating code by clicking on the eye icon that's displayed to the left of the cell when hovering.

Pluto's commands can be shown by pressing F1, but to run a cell you will typically use Shift-Enter or Ctrl-Enter."

# ╔═╡ e2e99276-536a-11eb-0104-b75480931fe4
md"## Packages
You probably want to use these. See below for more info."

# ╔═╡ 1b933b88-536b-11eb-2c77-efcc31ee8b8a
md"## Symbolic math
The SymEngine package isn't all that compatible with Pluto, but the basic functionality is through the `@vars` shorthand, which defines a variable as symbolic. Pluto will complain if you try to do it twice."

# ╔═╡ 43af754e-536b-11eb-15fe-1b3cd1d54554
@vars a b c d

# ╔═╡ 494b58c8-536b-11eb-3184-95220f0aaf07
S = [a b; c d]

# ╔═╡ 4f1c271c-536b-11eb-08a8-497bffc052f4
det(S)

# ╔═╡ 5bec3822-536b-11eb-1d59-9b8b97c3f476
md"#### Example: Quadratic forms"

# ╔═╡ 6c1ff2bc-536b-11eb-2603-f9ac90857a73
@vars x₁ x₂

# ╔═╡ 757bf162-536b-11eb-1c5a-0d5a6deb8422
x = [x₁, x₂]

# ╔═╡ 53deb4cc-536b-11eb-21ac-235dcfe31bcf
M = [a b; b c]

# ╔═╡ 7af47894-536b-11eb-0cbf-fd158015e157
# Usually, we would transpose a matrix or vector using M' syntax, however this only works for numerical matrices, not symbolic.
expand(transpose(x) * M * x)

# ╔═╡ 9b47a954-536b-11eb-1542-398fcd87e92d
md"## General linear algebra functions
### Matrix manipulation"

# ╔═╡ a74fa7ea-536b-11eb-3c05-83cab099aee4
# Get a random 3x3 matrix with integers from -100 to 100
A = rand(-100:100, 3,3)

# ╔═╡ d798c832-536b-11eb-218c-0d38e427e299
# Take the upper-left triangle of A and make a symmetric matrix
Symmetric(A)

# ╔═╡ e5168ba4-536b-11eb-309c-bd8fbb8e398e
# Take only the diagonal of A
Diagonal(A)

# ╔═╡ eeafd346-536b-11eb-1098-991929154bca
# Triangular (see also LowerTriangular)
UpperTriangular(A)

# ╔═╡ 0fd6b454-536c-11eb-0df9-a767c41168b0
md"### Properties"

# ╔═╡ 14304a9c-536c-11eb-2eea-49e73668f9dd
eigen(A)

# ╔═╡ 1b12ee28-536c-11eb-0d71-0382f6aed6fd
# Provided by the RowEchelon package
rref(A)

# ╔═╡ 25a9855e-536c-11eb-10cf-abd5ca96ec7a
inv(A)

# ╔═╡ 2b4cba12-536c-11eb-26a5-1f9fa6bfda6c
det(A)

# ╔═╡ 2da1eef4-536c-11eb-2d4e-4f0a24afc5a3
rank(A)

# ╔═╡ 385b54a0-536c-11eb-2a80-456c4d6d8dac
# Trace, not transpose
tr(A)

# ╔═╡ 5ff740b6-536c-11eb-0e7f-b16e515eb1ba
# Most random matrices will be invertible
nullspace(A)

# ╔═╡ 769cdc36-536c-11eb-393f-b50ce85bc2e6
norm(A)

# ╔═╡ 99f63092-536c-11eb-28ca-c3772327b63c
# The eachcol/eachrow iterators can be handy.
v, u = eachcol(rand(3, 2))

# ╔═╡ d0310600-536c-11eb-18fa-875713c27e48
collect(v)

# ╔═╡ d8f16c58-536c-11eb-30ab-73178da6e2d2
collect(u)

# ╔═╡ a417e426-536c-11eb-2afa-6bea7dcf238a
# Write \cdot (LaTeX), and press TAB.
v⋅u

# ╔═╡ dcc1f33e-536c-11eb-235f-5f1a109178ec
# Write \times (LaTeX), and press TAB.
v×u

# ╔═╡ Cell order:
# ╟─a8af804a-536a-11eb-0659-63c5920aafa2
# ╟─e2e99276-536a-11eb-0104-b75480931fe4
# ╠═ecd4cd68-536a-11eb-05a1-03f742d58318
# ╟─1b933b88-536b-11eb-2c77-efcc31ee8b8a
# ╠═43af754e-536b-11eb-15fe-1b3cd1d54554
# ╠═494b58c8-536b-11eb-3184-95220f0aaf07
# ╠═4f1c271c-536b-11eb-08a8-497bffc052f4
# ╟─5bec3822-536b-11eb-1d59-9b8b97c3f476
# ╠═6c1ff2bc-536b-11eb-2603-f9ac90857a73
# ╠═757bf162-536b-11eb-1c5a-0d5a6deb8422
# ╠═53deb4cc-536b-11eb-21ac-235dcfe31bcf
# ╠═7af47894-536b-11eb-0cbf-fd158015e157
# ╟─9b47a954-536b-11eb-1542-398fcd87e92d
# ╠═a74fa7ea-536b-11eb-3c05-83cab099aee4
# ╠═d798c832-536b-11eb-218c-0d38e427e299
# ╠═e5168ba4-536b-11eb-309c-bd8fbb8e398e
# ╠═eeafd346-536b-11eb-1098-991929154bca
# ╟─0fd6b454-536c-11eb-0df9-a767c41168b0
# ╠═14304a9c-536c-11eb-2eea-49e73668f9dd
# ╠═1b12ee28-536c-11eb-0d71-0382f6aed6fd
# ╠═25a9855e-536c-11eb-10cf-abd5ca96ec7a
# ╠═2b4cba12-536c-11eb-26a5-1f9fa6bfda6c
# ╠═2da1eef4-536c-11eb-2d4e-4f0a24afc5a3
# ╠═385b54a0-536c-11eb-2a80-456c4d6d8dac
# ╠═5ff740b6-536c-11eb-0e7f-b16e515eb1ba
# ╠═769cdc36-536c-11eb-393f-b50ce85bc2e6
# ╠═99f63092-536c-11eb-28ca-c3772327b63c
# ╠═d0310600-536c-11eb-18fa-875713c27e48
# ╠═d8f16c58-536c-11eb-30ab-73178da6e2d2
# ╠═a417e426-536c-11eb-2afa-6bea7dcf238a
# ╠═dcc1f33e-536c-11eb-235f-5f1a109178ec
