### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ 916299ca-5367-11eb-14d8-214592be7ef7
# Import Unitful.jl
using Unitful

# ╔═╡ 525602e6-536a-11eb-1d3b-1b4561d30e80
using Plots

# ╔═╡ bd624c10-5366-11eb-0db7-8f839805100e
md"# Pluto Quick-start
For use in SK1104, and similar courses.

The first thing you might ask, is how to write text or equations. The easiest way to accomplish that is to make a new cell and use the `md\"MARKDOWN\"` syntax, where MARKDOWN is replaced with your text in markdown. In this mode, MathJax is supported, so by wrapping your equation in \"\$\", you can write LaTeX. Typically you will then hide the generating code by clicking on the eye icon that's displayed to the left of the cell when hovering.

Pluto's commands can be shown by pressing F1, but to run a cell you will typically use Shift-Enter or Ctrl-Enter."

# ╔═╡ 6533be6a-5367-11eb-1da1-b36fccab841a
md"## Unitful package
For SK1104, and similar courses, I would suggest the `Unitful` package that provides basic unit conversions. Note that certain units, like radians (`rad`) are treated as if they were unitless."

# ╔═╡ a257090a-5367-11eb-3099-a98f85372fde
md"Once loaded, units can be defined using the following syntax below. The `|>` is a piping operator, just like the one in bash, and is handy when doing unit conversions. Adding a dot before an infix operator (like `.|>`) will cause it to broadcast over list-like structures."

# ╔═╡ 984f2d0c-5367-11eb-1298-793a490be246
# Note that Unitful treats h as the Planck constant, if you're into natural units
# Hence we use hr for hours
v = 10u"km/hr"

# ╔═╡ fb3885bc-5367-11eb-2ae9-71d6b449db5c
# Basic unit conversion.
v |> u"m/s"

# ╔═╡ 843e6fca-5368-11eb-36a4-f5bcdc3f16c4
# If you don't like fractions
v |> u"m/s" |> Float64

# ╔═╡ bb8e2d8a-5368-11eb-39c4-1574f151bc71
md"## Trigonometry
π and other LaTeX symbols can be retrieved by writing eg. `\pi`, and then pressing TAB."

# ╔═╡ 46c75f70-5369-11eb-0812-ff68fda4f56e
# Sine, using degrees
sind(30)

# ╔═╡ 4e68d16e-5369-11eb-08da-d177f460b802
# Sin, using radians
sin(π/2)

# ╔═╡ 62c0016e-5369-11eb-0154-354a94b040a5
# Inverses, here using degrees as output
asind(1)

# ╔═╡ 8db3b0b4-5369-11eb-328e-1b4686f5d923
# Sometimes you don't want to write pi. cospi(x) = cos(πx)
cospi(1)

# ╔═╡ a7572fdc-5369-11eb-109c-73ded26426c5
md"## Animations
Animations are easily made using the `Plots` package, with the `@gif` shorthand in conjunction with a `for`-loop. Check the Plots.jl documentation for advanced usage. 

* `plot()` - Creates a new plot, and attempts to plot whatever you throw at it.
* `plot!()` - Similar to `plot()`, but the ! is Julia convention for overwriting or altering something, in this case it will add stuff to the previous plot.
"

# ╔═╡ 03e6fde2-536a-11eb-0d1e-c58351fd2db5
@gif for t in 0:0.1:2
	plot(x -> sinpi(x-t), label = "t = $t")
end

# ╔═╡ Cell order:
# ╟─bd624c10-5366-11eb-0db7-8f839805100e
# ╟─6533be6a-5367-11eb-1da1-b36fccab841a
# ╠═916299ca-5367-11eb-14d8-214592be7ef7
# ╠═a257090a-5367-11eb-3099-a98f85372fde
# ╠═984f2d0c-5367-11eb-1298-793a490be246
# ╠═fb3885bc-5367-11eb-2ae9-71d6b449db5c
# ╠═843e6fca-5368-11eb-36a4-f5bcdc3f16c4
# ╟─bb8e2d8a-5368-11eb-39c4-1574f151bc71
# ╠═46c75f70-5369-11eb-0812-ff68fda4f56e
# ╠═4e68d16e-5369-11eb-08da-d177f460b802
# ╠═62c0016e-5369-11eb-0154-354a94b040a5
# ╠═8db3b0b4-5369-11eb-328e-1b4686f5d923
# ╟─a7572fdc-5369-11eb-109c-73ded26426c5
# ╠═525602e6-536a-11eb-1d3b-1b4561d30e80
# ╠═03e6fde2-536a-11eb-0d1e-c58351fd2db5
