### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 97ae3c18-4f42-11eb-08b3-0993a9f6da84
begin
	using Plots
	Plots.gr()
	Plots.theme(:bright)
end

# ╔═╡ 551b163c-4f42-11eb-044f-4374c4ba4efb
md"Set n₂:"

# ╔═╡ 755f1ef6-4f2a-11eb-2b16-7f5c93be2dbe
@bind n2 html":<input type=range min=1 max=3 step=0.1 value=1.5>"

# ╔═╡ aedc130a-4f3e-11eb-30df-1908a6fce61b
begin
	plot(legend = :left,
		xlabel = "θᵢ (°)", ylabel = "T", ylims = (0, 1),
		title = "n₂ = $n2"
	)
	plot!(θi -> (tand(θi-asind(sind(θi)/n2))/tand(θi+asind(sind(θi)/n2)))^2, xlims = (0,90), label = "Rₚ")
	plot!(θi -> (sind(θi-asind(sind(θi)/n2))/sind(θi+asind(sind(θi)/n2)))^2, xlims = (0,90), label = "Rₛ")
	plot!(θi -> 1-(tand(θi-asind(sind(θi)/n2))/tand(θi+asind(sind(θi)/n2)))^2, xlims = (0,90), label = "Tₚ")
	plot!(θi -> 1-(sind(θi-asind(sind(θi)/n2))/sind(θi+asind(sind(θi)/n2)))^2, xlims = (0,90), label = "Tₛ")
	vline!([atand(n2/1)], label = "Brewstervinkeln")
end

# ╔═╡ ca319a42-4f42-11eb-3f39-6d48cf0f0755
md"I exempeluppgift 47 är T _nästan_ T för rätvinkligt infall i kvadrat. Detta är inte det generella fallet:"

# ╔═╡ 899279c8-4f29-11eb-1daf-4971a70ce6ac
function f(θi, n2)
	θb = asind(sind(θi)/n2)
	R_p = (tand(θi-θb)/tand(θi+θb))^2
	R_s = (sind(θi-θb)/sind(θi+θb))^2
	T_p = 1-R_p
	T_s = 1-R_s
	return (T_s + T_p)/2
end

# ╔═╡ ff16a374-4ed2-11eb-2943-d33fa88bbe19
begin
	plot(legend = :bottomleft,
		xlabel = "θᵢ (°)", ylabel = "T", ylims = (0, 1),
		title = "n₂ = $n2"
	)
	hline!([(4*1*n2)/((1+n2)^2)], label = "T") #hline since it does not depend on θi
	plot!(0:0.1:90, f.(0:0.1:90, n2), label = "T, Fresnel")
	vline!([atand(n2/1)], label = "Brewstervinkeln")
end

# ╔═╡ 5f0614f0-4f2c-11eb-103f-73c40d23d760
begin
	local xrange = 1:0.01:4
	plot(legend = :bottomleft, xlabel = "n₂", ylabel = "T", ylims = (0, 1))
	plot!(xrange, n2 -> (4*1*n2)/((1+n2)^2), label = "T")
	plot!(xrange, n2 -> ((4*1*n2)/((1+n2)^2))^2, label = "T²")
	plot!(xrange, n2 -> f(atand(n2/1), n2), label = "T, Fresnel, Brewster")
end

# ╔═╡ Cell order:
# ╠═97ae3c18-4f42-11eb-08b3-0993a9f6da84
# ╟─551b163c-4f42-11eb-044f-4374c4ba4efb
# ╟─755f1ef6-4f2a-11eb-2b16-7f5c93be2dbe
# ╟─aedc130a-4f3e-11eb-30df-1908a6fce61b
# ╟─ff16a374-4ed2-11eb-2943-d33fa88bbe19
# ╟─ca319a42-4f42-11eb-3f39-6d48cf0f0755
# ╟─5f0614f0-4f2c-11eb-103f-73c40d23d760
# ╟─899279c8-4f29-11eb-1daf-4971a70ce6ac
