### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ cf216a96-5026-11eb-0ac9-0da662324dde
using Unitful

# ╔═╡ dfe0dd8a-5026-11eb-01e3-99bb2e35e7cc
f = 256u"Hz"

# ╔═╡ f28e8504-5026-11eb-0331-1b51c7f06e91
md"Ljudhastigheten i en sträng ges av:

$$v = \sqrt{\frac{F_s}{\mu}}$$

Där $\mu$ är massan per enhet längd hos snöret och $F_s$ är spännkraften. Antaget försumbar ändring av $\mu$ får vi:

$$v \propto \sqrt F$$

Om F ökar med 3% ökar således v med $\sqrt {3\%}$. $v=\lambda f$, och om $\lambda$ är konstant innebär detta att även f måste öka med $\sqrt {3\%}$. Tänk på att vi vill räkna med förändringsfaktorer.

Vi kommer att få en svävning med denna frekvens, samt den för stämgaffeln.
"

# ╔═╡ 54b351ba-5027-11eb-29a4-8384a3aa3e8f
f_2 = f*(sqrt(1.03))

# ╔═╡ 5f0b1344-5029-11eb-12ed-3997b9ce8d3b
# Svävningsfrekvensen
f_s = f_2-f

# ╔═╡ Cell order:
# ╠═cf216a96-5026-11eb-0ac9-0da662324dde
# ╠═dfe0dd8a-5026-11eb-01e3-99bb2e35e7cc
# ╟─f28e8504-5026-11eb-0331-1b51c7f06e91
# ╠═54b351ba-5027-11eb-29a4-8384a3aa3e8f
# ╠═5f0b1344-5029-11eb-12ed-3997b9ce8d3b
