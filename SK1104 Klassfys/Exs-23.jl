### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ a5c543d8-5029-11eb-2b19-999a69b0216d
using Plots

# ╔═╡ e783db88-502f-11eb-023f-67504cf35b81
md"Vi får en slags stående våg, men amplituden i noderna är inte 0. Nedan visas ett exempel med påhittade värden på reflektansen."

# ╔═╡ ad8a3cf2-5029-11eb-1bae-47324887cb36
@gif for t ∈ 0:0.02:2
	plot(xlims = (-3.5, 0), ylims = (-2, 2),
		 size = (700, 250), legend = :outerright)
	plot!(x -> sinpi(x-t),
		 line = :dash, label = "Inkommande våg")
	plot!(x -> 0.7sinpi(-x-t),
		 line = :dash, label = "Reflekterad våg")
	plot!(x -> sinpi(x-t) + 0.7sinpi(-x-t),
		label = "Σ")
	hline!([-0.3, 0.3],
		label = "Pₘᵢₙ", line = :dot, color = :5)
	hline!([-1.7, 1.7],
		label = "Pₘₐₓ", line = :dot, color = :4)
	vline!((-3:0) .- 0.5,
		label = "Nod", line = :dot, color =:5)
	vline!(-3:0,
		label = "Buk", line = :dot, color = :4)
end

# ╔═╡ 98afe472-5030-11eb-3a9e-b560b0853055
md"Uppgiften ger:"

# ╔═╡ 8a7b308e-502b-11eb-14a1-434b2c8e05d8
Pₘₐₓ = 100

# ╔═╡ 9169025e-502b-11eb-268b-dfad2b42aa07
Pₘᵢₙ = 28

# ╔═╡ e499ea60-502b-11eb-1d63-37e69406b648
md"Trycket motsvarar amplituderna, vilka adderas respektive subtraheras vid interferens då fasskillnaden är 0 eller $\pi$. Detta ger:

$$P_{max} = P_i + P_r$$

$$P_{min} = P_i - P_r$$
"

# ╔═╡ 6abd0bba-502d-11eb-19f1-3b885340b5f0
P_i = (Pₘₐₓ+Pₘᵢₙ)//2

# ╔═╡ 78bb5dea-502d-11eb-3372-299f69a1287b
P_r = (Pₘₐₓ-Pₘᵢₙ)//2

# ╔═╡ 82f090c8-502d-11eb-1399-fb31a50071cc
md"$$\alpha = \frac{I_i - I_r}{I_i} = \frac{P_i^2 - P_r^2}{P_i^2}$$

Absorbtionskoefficienten blir således:"

# ╔═╡ b1fb16fe-502d-11eb-18d5-0daab030a9bb
α = (P_i^2 - P_r^2)//(P_i^2)

# ╔═╡ 18173842-5032-11eb-17dc-3d08476105f1
α |> Float64

# ╔═╡ Cell order:
# ╠═a5c543d8-5029-11eb-2b19-999a69b0216d
# ╟─e783db88-502f-11eb-023f-67504cf35b81
# ╠═ad8a3cf2-5029-11eb-1bae-47324887cb36
# ╟─98afe472-5030-11eb-3a9e-b560b0853055
# ╠═8a7b308e-502b-11eb-14a1-434b2c8e05d8
# ╠═9169025e-502b-11eb-268b-dfad2b42aa07
# ╟─e499ea60-502b-11eb-1d63-37e69406b648
# ╠═6abd0bba-502d-11eb-19f1-3b885340b5f0
# ╠═78bb5dea-502d-11eb-3372-299f69a1287b
# ╟─82f090c8-502d-11eb-1399-fb31a50071cc
# ╠═b1fb16fe-502d-11eb-18d5-0daab030a9bb
# ╠═18173842-5032-11eb-17dc-3d08476105f1
