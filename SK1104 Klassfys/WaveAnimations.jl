### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ aa5ab236-4f3d-11eb-3798-f77337e14dea
begin
	using Plots
	Plots.gr()
	Plots.theme(:juno)
end

# ╔═╡ 949ff842-4f4f-11eb-0b7c-d3d5c7823d54
md"# Animationer av vågor
Inställningar för grafning. För tillgängliga teman, se [PlotThemes](https://github.com/JuliaPlots/PlotThemes.jl)."

# ╔═╡ ac68e10a-4f4f-11eb-38b2-dd3b88a8170e
md"## Vågens riktning
Vågens utbredningsriktning ges enkelt genom att sätta $kx+\omega t$ som konstant, och observera vad som händer när t ökar (vi går framåt i tiden).
I fallet nedan har vi $cos(x+t)$, dvs. $x$ och $t$ har samma tecken, vilket innebär att om den ena ökar, måste den andra minska.
Om vi ökar $t$, måste vi minska $x$, och vågen rör sig alltså i den negativa $x$-axelns riktning."

# ╔═╡ 96506302-4f3b-11eb-0488-5bcc92a14740
begin
	local anim = @animate for t in -2π:π/50:2π
	    plot(title = "t=$(round(t, digits = 1))", xlabel = "x")
	    plot!(x -> cos(x+t), label = "y = cos(x+t)", xlims = (-6.3, 6.3))
	    vline!((-4π:π:4π) .- t, label = "x ∈ {nπ-t : n∈ℤ}")
	    vline!([-t], label = "x = -t")
	end
	gif(anim, fps = 30, show_msg = false)
end

# ╔═╡ e1934afa-4f4f-11eb-2cbe-cfc9879973c1
md"## Stående vågor"

# ╔═╡ a057d93e-4f3b-11eb-10c0-1b6e510efeaa
begin
	local anim = @animate for t in -1:0.01:1
	    plot(title = "Stående vågor", xlabel = "x",
	        xlims = (-1, 1), ylims = (-2, 2),
	        size = (961, 480), title_location = :center,
	        legend = :outerright,
	    )
	    # Waves
	    plot!(x -> cospi(x+t), label = "y₁ = cos(π(x+t))")
	    plot!(x -> -cospi(x-t), label = "y₂ = -cos(π(x-t))")
	    plot!(x -> cospi(x+1-t) + cospi(x+t), label = "yₛ = y₁ + y₂")
	    
	    # ΔΦ lines
	    ΔΦline = (:dash, 0.7)
	    hline!([cospi(t+0.5), -cospi(t+0.5)], label = "Φ = 0", line = ΔΦline, color = :4)
	    hline!([cospi(t), -cospi(t)], label = "Φ = π", line = ΔΦline, color = :5)
	    vline!([0.5, -0.5], label = false, line = ΔΦline, color = :4)
	    vline!(-1:1, label = false, line = ΔΦline, color = :5)
	    
	    # Standing wave bounds
	    plot!(x -> 2sinpi(x), label = false, line = (:gray, :dot))
	    plot!(x -> -2sinpi(x), label = false, line = (:gray, :dot))
	end
	gif(anim, fps = 30, show_msg = false)
end

# ╔═╡ a6319bf8-4f3b-11eb-014b-ef16ec06fdff
md"## Svävning"

# ╔═╡ 1ecfbe4e-501d-11eb-1bdc-2b039c0daeeb
@bind μ html"μ<input type=range min=0 value=2 max=5>"

# ╔═╡ af092d8e-4f3b-11eb-3656-c5090dc31e64
begin
	local anim = @animate for t in -2π:π/30:2π
	    plot(title = "t=$(round(t, digits = 1))", xlabel = "x", ylims = (-2,2))
	    plot!(x -> cos(x+t), label = "y = cos(x+t)", xlims = (-6.3, 6.3))
	    plot!(x -> cos(x+μ*t), label = "y = cos(x+μt)", xlims = (-6.3, 6.3))
	    plot!(x -> cos(x+μ*t)+cos(x+t), label = "Σ", xlims = (-6.3, 6.3))
	end
	gif(anim, fps = 30, show_msg = false)
end

# ╔═╡ 321dfec0-4f50-11eb-26c5-5d42b4d80b4e
md"## Olika sorters polarisation
Representerade som en summa av två polariserade vågor med fasskillnad på formen $\frac{n\pi}{4}$. I SK1104 definieras cirkulär polarisation utblickandes i strålgångens riktning i ett högerhänt koordinatsystem."

# ╔═╡ 314b8832-4f50-11eb-3e22-b75a81198e8f
# X range
x = 0:0.04:4

# ╔═╡ 89f3d24c-4f3b-11eb-2732-67f7588fa3be
begin
	local anim = @animate for t in 0:0.02:2
		ps = []
		for (title, Φ, Φrep) in [("Vänsterpolariserad", 1/2, "π/2"),
								("Högerpolariserad", -1/2, "-π/2"),
								("Linjärpolariserad, motfas", 1, "π"),
								("Linjärpolariserad, i fas", 0, "0")]
			plot(title=title, xlabel = "x", ylabel = "y", zlabel = "z")
			plot3d!(x, cospi.(x .- t), zeros(size(x)), label = "ϕ = 0", line = :dot)
			plot3d!(x, zeros(size(x)), cospi.(x .- t .+ Φ), label = "ϕ = $Φrep", line = :dot)
			plot3d!(x, cospi.(x .- t), cospi.(x .- t .+ Φ), label = "Σ")
			push!(ps, plot!())
		end

		plot(ps..., size = (500, 400) .* 2)
	end
	gif(anim, fps = 30, show_msg = false)
end

# ╔═╡ 8767a5ac-4f50-11eb-0f29-ffe30e09f237
md"## Ännu fler sorters polarisation
Om vi låter den ena vågen vara statisk ovan, kan vi variera fasskillnaden, och observera resultatet.
Notera att 2D-grafen har en \"bakvänd\" y-axel, eftersom vi betraktar yz-planet, från x-axelns negativa riktning, åt det håll dit vågen färdas (positiv x-axel). Det hade egentligen gått att betrakta vågen från andra hållet, om man byter riktning på de två pilarna vid $z=0$. Om vi har en våg, och stannar tiden, kommer dess cirkulärpolarisation vara samma oavsett från vilket håll den betraktas."

# ╔═╡ a03353e2-4f50-11eb-2308-89e2ba391b4a
begin
	local anim = @animate for t ∈ (-1:0.01:1)[2:end]
	    # Kodupprepning <3
	    polarity = ((t%1 == 0) ? 0 : (t < 0) ? -1 : 1)
	    poldesc =  ["Höger","Linjär","Vänster"][polarity+2];
	    
	    # 3D Plot
	    p1 = plot(title="ΔΦ = $t π", xlabel = "x", ylabel = "y", zlabel = "z")
	    
	    # Moving wave
	    plot3d!(x, cospi.(x .- t), cospi.(x), label = "Σ")
	    
	    # Lines corresponding to the points on the 2D plot
	    plot3d!(x, cospi.(zeros(size(x)).-t), ones(size(x)), label = false, color = :2)
	    plot3d!(x, -cospi.(zeros(size(x)).-t), -ones(size(x)), label = false, color = :3)
	    
	    
	    plot3d!(x, -sinpi.(zeros(size(x)).-t), zeros(size(x)), label = false, color = :4)
	    plot3d!(x, sinpi.(zeros(size(x)).-t), zeros(size(x)), label = false, color = :5)
	    
	    # Waves
	    plot3d!(x, cospi.(x .- t), zeros(size(x)), label = "E₁")
	    plot3d!(x, zeros(size(x)), cospi.(x), label = "E₂")
	    
	    # Helper lines
	    plot3d!([0,0], [-1,1], [1,1], label = false, color = :gray)
	    plot3d!([0,0], [-1,1], [-1,-1], label = false, color = :gray)
	    plot3d!([0,4], [0,0], [0,0], label = false, color = :gray)
	    
	    # 2D Plot
	    p2 = plot(title="2D-Projektion i utbredningsriktningen", xlabel = "y", ylabel = "z", xflip=true)
	    plot!(cospi.(x .- t), cospi.(x), label = poldesc)
	    
	    # Points corresponding to the lines on the 3D plot
	    scatter!([cospi(-t)], [1], label = false, color = :2, marker = (6, [:rtriangle, :x, :ltriangle][polarity+2]))
	    scatter!([-cospi(-t)], [-1], label = false, color = :3, marker = (6, [:ltriangle, :x, :rtriangle][polarity+2]))
	    scatter!([-sinpi(-t)], [0], label = false, color = :4, marker = :dtriangle)
	    scatter!([sinpi(-t)], [0], label = false, color = :5, marker = :utriangle)
	
	
	    plot(p1, p2,  size = (1000, 500))
	end
	gif(anim, fps = 30, show_msg = false)
end

# ╔═╡ Cell order:
# ╟─949ff842-4f4f-11eb-0b7c-d3d5c7823d54
# ╠═aa5ab236-4f3d-11eb-3798-f77337e14dea
# ╟─ac68e10a-4f4f-11eb-38b2-dd3b88a8170e
# ╟─96506302-4f3b-11eb-0488-5bcc92a14740
# ╟─e1934afa-4f4f-11eb-2cbe-cfc9879973c1
# ╟─a057d93e-4f3b-11eb-10c0-1b6e510efeaa
# ╟─a6319bf8-4f3b-11eb-014b-ef16ec06fdff
# ╠═1ecfbe4e-501d-11eb-1bdc-2b039c0daeeb
# ╟─af092d8e-4f3b-11eb-3656-c5090dc31e64
# ╟─321dfec0-4f50-11eb-26c5-5d42b4d80b4e
# ╠═314b8832-4f50-11eb-3e22-b75a81198e8f
# ╠═89f3d24c-4f3b-11eb-2732-67f7588fa3be
# ╟─8767a5ac-4f50-11eb-0f29-ffe30e09f237
# ╟─a03353e2-4f50-11eb-2308-89e2ba391b4a
