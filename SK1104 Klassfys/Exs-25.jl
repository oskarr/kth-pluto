### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ 98cbcb98-5022-11eb-395b-37e6b7cc641b
using Unitful

# ╔═╡ 56d41398-5023-11eb-0b98-675552f4cb07
P_0 = 2e-5u"Pa"

# ╔═╡ a4bf5366-5022-11eb-3f22-59cc739c3907
P_max = 10^(80/20) * P_0

# ╔═╡ ba1e5216-5022-11eb-3df5-abf5574d27cd
P_min = 10^(55/20) * P_0

# ╔═╡ add5cb50-5025-11eb-2a0f-993f7b04ad5d
md"Det kan vara frestande att använda

$$A^2 = A_1^2 + A_2^2 + 2A_1 A_2cos(\phi)$$

Men eftersom vi enbart betraktar $cos\phi = \pm 1$, kan vi skriva om det som

$$A^2 = (A_1 \pm A_2)^2$$

Alltså har vi att (P motsvarar effektivtrycket, vilket är proportionellt mot $\Delta P_{max}$, vilket motsvarar amplituden). Om det endast är kvoten vi söker kan vi dock konstatera att effektivtrycket och maxtrycket torde ge samma resultat. Vi kan konstatera att:

$$P_{max} = P_1 + P_2$$

$$P_{min} = P_1 - P_2$$
"

# ╔═╡ befea68c-5022-11eb-0124-654c2c930037
P_1 = (P_min+P_max)/2

# ╔═╡ 254c308a-5023-11eb-1a40-f5cc5adcd222
P_2 = (P_max-P_min)/2

# ╔═╡ 42b70caa-5023-11eb-0123-3f7d032a7ead
# Den sökta kvoten
P_1/P_2

# ╔═╡ dbdd8c14-5022-11eb-221c-294a48159e3c
f_1 = 880u"Hz"

# ╔═╡ fc5e587e-5022-11eb-2f48-c397a0bbbcd1
f_s = 10/25u"s" |> u"Hz"

# ╔═╡ 1b45c448-5023-11eb-2dd2-c38fa07bcf35
# Möjliga frekvenser för f_2
f_1 .+ [f_s, -f_s]

# ╔═╡ Cell order:
# ╠═98cbcb98-5022-11eb-395b-37e6b7cc641b
# ╠═56d41398-5023-11eb-0b98-675552f4cb07
# ╠═a4bf5366-5022-11eb-3f22-59cc739c3907
# ╠═ba1e5216-5022-11eb-3df5-abf5574d27cd
# ╟─add5cb50-5025-11eb-2a0f-993f7b04ad5d
# ╠═befea68c-5022-11eb-0124-654c2c930037
# ╠═254c308a-5023-11eb-1a40-f5cc5adcd222
# ╠═42b70caa-5023-11eb-0123-3f7d032a7ead
# ╠═dbdd8c14-5022-11eb-221c-294a48159e3c
# ╠═fc5e587e-5022-11eb-2f48-c397a0bbbcd1
# ╠═1b45c448-5023-11eb-2dd2-c38fa07bcf35
