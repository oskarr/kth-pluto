### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# ╔═╡ 540c364a-4c5e-11eb-1b6f-f31bf1c25e12
using Unitful 

# ╔═╡ 761d5108-4c69-11eb-18f7-6984df2d27f7
f = -2.4u"cm"

# ╔═╡ 10a64db8-4c5f-11eb-222f-0d8ab359dcbf
s = 4u"cm"

# ╔═╡ 6054c462-4c5e-11eb-00ab-f15e3794926a
sp = 1/(1/f - 1/s)

# ╔═╡ c11bb202-4c64-11eb-2ca8-758084a5ab7d
d = 10u"cm"-sp

# ╔═╡ 82b89b5e-4c64-11eb-3d0e-9b11a63af09b
h = 20.8u"cm"/d * -sp

# ╔═╡ f5a1808e-4c5e-11eb-3896-9351155242c1
θ2 = atand(h/(2*s))

# ╔═╡ 3cd343e2-4c6d-11eb-1977-c7ffb56758a0
atand(sind(θ2)/1.49)

# ╔═╡ dfd76d8a-4c6c-11eb-30e4-ffea80e7a9ab
n_k = sqrt(sind(θ2)^2+1.49^2)

# ╔═╡ 686b25a4-4c60-11eb-3905-e3ccc3cada50
θR = asind(sind(θ2)/n_k)

# ╔═╡ 80bb9262-4c63-11eb-0a49-b5bd7a3030a5
acosd(1.49/n_k)

# ╔═╡ f619f1da-4c6c-11eb-2254-b55fcdc0a68d
asind(sind(θ2)/n_k)

# ╔═╡ Cell order:
# ╠═540c364a-4c5e-11eb-1b6f-f31bf1c25e12
# ╠═761d5108-4c69-11eb-18f7-6984df2d27f7
# ╠═10a64db8-4c5f-11eb-222f-0d8ab359dcbf
# ╠═6054c462-4c5e-11eb-00ab-f15e3794926a
# ╠═c11bb202-4c64-11eb-2ca8-758084a5ab7d
# ╠═82b89b5e-4c64-11eb-3d0e-9b11a63af09b
# ╠═3cd343e2-4c6d-11eb-1977-c7ffb56758a0
# ╠═f5a1808e-4c5e-11eb-3896-9351155242c1
# ╠═686b25a4-4c60-11eb-3905-e3ccc3cada50
# ╠═80bb9262-4c63-11eb-0a49-b5bd7a3030a5
# ╠═dfd76d8a-4c6c-11eb-30e4-ffea80e7a9ab
# ╠═f619f1da-4c6c-11eb-2254-b55fcdc0a68d
