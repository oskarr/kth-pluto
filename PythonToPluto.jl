### A Pluto.jl notebook ###
# v0.12.18

using Markdown
using InteractiveUtils

# ╔═╡ 99a8eb1c-5370-11eb-322a-118b38e8552d
# The DataFrames package provides a very nice Not-selector
using DataFrames

# ╔═╡ 3aedfbba-536d-11eb-2727-81c5c323f11a
md"# Quick-start
## What is Pluto?
Pluto is similar to Jupyter, in the sense that it provides a cell-based programming environment. The principal difference is that Pluto figures out the order-of-execution of cells, whereas Jupyter does not. This means that the notebook you're using is always in a \"fresh\" state, but also constrains you in the sense that each variable can only be defined in one cell.

## Pluto
The first thing you might ask, is how to write text or equations. The easiest way to accomplish that is to make a new cell and use the `md\"MARKDOWN\"` syntax, where MARKDOWN is replaced with your text in markdown. In this mode, MathJax is supported, so by wrapping your equation in \"\$\", you can write LaTeX. Typically you will then hide the generating code by clicking on the eye icon that's displayed to the left of the cell when hovering.

Pluto's commands can be shown by pressing F1, but to run a cell you will typically use Shift-Enter or Ctrl-Enter."

# ╔═╡ 85c9e090-536d-11eb-1acd-a34aedb79bd8
md"## Julia
Pluto is based on the [Julia language](https://julialang.org), which is very Python-like, hence this file is aimed primarily at readers who are already familiar with Python. A lot of things are exactly the same.

## Common mistakes"

# ╔═╡ d78c14c8-5371-11eb-09cc-1f4278ba4954
# Julia is 1-indexed
[1, 2, 3][1]

# ╔═╡ 58ba690e-5373-11eb-0532-e5e702f55d95
# While Julia normally supports print and println, Pluto does not.
# This won't show anything here, and usually not in the console where you ran Pluto.
print("What??")

# ╔═╡ 021ccd5a-5377-11eb-3c5f-77b788c97a3b
# Ranges are constructed like slices in Python, and are inclusive
collect(0:0.7:3.5)

# ╔═╡ eff86c24-5376-11eb-25f8-b53e4db436ac
# Julia uses `end`, not indentation
if true
	"Don't indent"
end

# ╔═╡ e116241e-5371-11eb-20b1-b56cb3fdfb92
# There's no len(function). It's called length here. If length doesn't work, try size.
length(rand(20))

# ╔═╡ 0d1a6cd6-5375-11eb-382e-810bc6bc102b
md"## Flow control
Is very similar to that of Python. The principal difference is that we don't use indentation, but rather the `end` keyword. Julia is also waaay more sensitive to using types other than booleans in these statements. Also, Julia does not support `for-else` and `while-else`."

# ╔═╡ bb60bafa-5375-11eb-2168-6f3c0813321b
b1 = false

# ╔═╡ ab4cc898-5375-11eb-2b32-c3870c66f035
if b1
	"Yay!"
elseif !b1
	"Nope!"
else
	"I'll never get my chance :("
end

# ╔═╡ 11ff0084-5375-11eb-2d7f-7b3ea1d3efe6
begin 
	local a = 0
	for n in [1,2,3]
		a += n
	end
	a
end

# ╔═╡ 3cc87d52-5375-11eb-1dad-03a744428e33
begin 
	local a = 0
	# Ranges are a lot more flexible than in Python.
	# This range yields [0.1, 2.1, 4.1] (Julia's ranges are inclusive)
	for n in 0.1:2:4.1
		a += n
	end
	a
end

# ╔═╡ 71f50c04-5375-11eb-16f8-f54418565c4e
begin 
	local v = [0, 0, 0]
	local M = [1 0 0 ; 0 1 0; 0 0 1] # 3x3 identity matrix
	# Loop over columns of a matrix
	for u in eachcol(M)
		v += u
	end
	v
end

# ╔═╡ d11e6a38-5371-11eb-0164-2f1156804f3b
md"### Basic types
#### Numbers"

# ╔═╡ c6e26f3e-536d-11eb-208f-6d22b83f5654
# Integers
1, typeof(1)

# ╔═╡ a939afd6-536d-11eb-1ed0-0b8893733c0a
# Floats
2.23, typeof(2.23)

# ╔═╡ d70a8e6e-536d-11eb-2624-bfeddda6346c
# Fractions (referred to as type Rational) 
2//3, typeof(2//3)

# ╔═╡ 00c3029a-536e-11eb-090e-d325ac6925a4
# BigInts. Since exponentials are evaluated right-to-left, we only need to set the last exponential to BigInt. The rest will be promoted to BigInt during execution.
2^2^2^2^BigInt(2)

# ╔═╡ 368d4fb8-536e-11eb-2618-5912a6ade66f
# Infinity
Inf

# ╔═╡ 5f0d7c0e-536e-11eb-03e1-9f82a7df928f
# Note that fraction division by 0 will yield a signed infinity on Float conversion
-1//0 |> Float64

# ╔═╡ 74ac2182-536e-11eb-0c58-53c6abb75c78
# Irrational numbers. Type \pi (or other LaTeX symbol), and then press TAB.
π, typeof(π)

# ╔═╡ 902e53da-536e-11eb-1675-1b767226d52e
# Complex numbers. Generally stored using Float64:s
1+2.0im, typeof(1+2.0im)

# ╔═╡ aafedeaa-536e-11eb-3db1-ab41bfcc15ad
# But we can also have Integers, Rationals, etc.
# Note that since we placed im in the denominator, we get a minus sign.
1//3 + 2//3im

# ╔═╡ c3564440-536e-11eb-38e0-e933b6bc2ef7
a = 2

# ╔═╡ c6e2e24a-536e-11eb-1ff1-3762ce31fb8a
# All variables can be concatenated to the right-hand side of a number. This will be treated as multiplication
2a

# ╔═╡ d8b07214-536e-11eb-3a49-0fdbfb3b5044
md"### List-like structures
The `LinearAlgebra` package provides additional Matrix types (eg. Diagonal, Symmetric), but that's beyond the scope of this file."

# ╔═╡ e40711e0-536e-11eb-2b1e-5155f1dfc315
# A list, just like in Python (mutable)
L1 = [1,2,3]

# ╔═╡ f704f154-536e-11eb-0231-3d462b18e7f7
begin
	# If you desperately need a 0-indexed array (or a 42-indexed one)
	# Note that the OffsetArray here becomes a view for L1, hence adding stuff to L2 will add stuff to L1, and vice versa
	using OffsetArrays
	L2 = OffsetArray(L1,0:2)
end

# ╔═╡ ed5c72e4-536e-11eb-361c-cbeb82b0c974
# Lists are 1-indexed, not 0-indexed
L1[1]

# ╔═╡ 942cb6ae-5376-11eb-2e31-8d34066d1048
# Ranges
collect(0:0.1:1)

# ╔═╡ 8d559e72-5376-11eb-1a75-cdc1bb21f661
# List comprehensions
[a for a in 0:1000 if a%3 == 1 && a%7==1]

# ╔═╡ 095dc54a-536f-11eb-3a43-9d7d3cd84c4a
L2[0]

# ╔═╡ 926a9732-5374-11eb-0495-652aed838a70
# Add a something to a list. The "!" signifies that we are modifying some parameter, in this case the list L2.
# Please note that Pluto does not go well with mutating objects in different cells.
# Also, L2 is a view of L1, and we'll be adding stuff to L1 when adding stuff to L1.
push!(L2, 4)

# ╔═╡ 309efd88-536f-11eb-2462-f1395e35ab52
# Tuples (immutable)
T1 = (1, 2, 3)

# ╔═╡ 593a1958-536f-11eb-2f69-8f1a190e80be
# Tuples (or any other iterable) are converted to vectors (1D arrays) using the collect function, and not the Vector() function.
collect(T1)

# ╔═╡ 7be75196-536f-11eb-123c-1772d25157a1
# In contrast, vectors can be cast to Tuples using the constructor (Tuple())
Tuple(L2)

# ╔═╡ 85c85956-5376-11eb-10bf-e9e730b8d222
md"#### Higher dimensionse"

# ╔═╡ 8bd68298-536f-11eb-2bd9-7f580489a6f6
# Julia supports 2D lists (matrices). They are constructed as follows, in a row-major order.
M1 = [1 2 3; 4 5 6; 7 8 9]

# ╔═╡ 4a7986c8-5370-11eb-3d8d-4d8b94422721
# Matrix indexing. The first is row, the second is column.
M1[1, 2]

# ╔═╡ 5736f742-5370-11eb-3a87-8d652d05da3d
# Grab a row (: means "all", and placed in the columns index, it will return all columns in the given row)
M1[1, :]

# ╔═╡ 6ab88194-5370-11eb-10c7-a50fc18af76d
# Grab a column
M1[:, 2]

# ╔═╡ 7e696f20-5370-11eb-05d1-f51d18f56286
# Remove row 2 and column 2
M1[[1, 3], [1, 3]]

# ╔═╡ 9454e03a-5370-11eb-06bf-6d955fe47ecf
M1[Not(2), Not(2)]

# ╔═╡ b38bb3f8-536f-11eb-0315-21350b60b1f3
# You could construct 7-dimensional arrays/tensors if you want...
# How about a 7-dimensional tensor with only one element?
M2 = ones(repeat([1], 7)...)

# ╔═╡ cd4d3e90-5376-11eb-00b4-f33df83df34a
# Reshaping
reshape(M2, 1, :)

# ╔═╡ 3323b3a4-5370-11eb-071f-9bdfd3b105a1
# Concatenation (vertical)
vcat(M1, M1)

# ╔═╡ 3f982142-5370-11eb-08fd-13ab37c4bf91
# Concatenation (horizontal)
hcat(M1, M1)

# ╔═╡ 13e4887c-5370-11eb-23bc-693ce7019932
md"### Strings"

# ╔═╡ 1856cd84-5370-11eb-22bb-e50730a81532
s1 = "Hello world"

# ╔═╡ 1d3a10c4-5370-11eb-185c-bde2a6017d47
# Repetition
s1^3

# ╔═╡ 234736cc-5370-11eb-0e6d-2bb0f67cd706
# Concatenation
s1 * "!"

# ╔═╡ 2dad88aa-5370-11eb-20a4-19d2855559bf
# Strings can mostly be treated as vectors
vcat(s1, s1)

# ╔═╡ cfc32d84-5370-11eb-0b75-7fad0fea5907
# All indices are inclusive
s1[3:5]

# ╔═╡ eb3b1914-5370-11eb-072d-cd7243ac7508
# end represents the index of the last char, and since indices are inclusive, this will return a string of length 5.
s1[end-4:end]

# ╔═╡ 0101e3c2-5371-11eb-0c15-c7d7135cec19
# Indexing can always be done with lists
s1[[2,2,7]]

# ╔═╡ 1a5016c0-5371-11eb-1a7d-0b8cbe195315
# All functions in Julia are stand-alone. We don't write s1.split()
split(s1)

# ╔═╡ 2a528290-5371-11eb-221f-49af4e3d2b51
# The same thing is true for join
join(split(s1), "-")

# ╔═╡ 423059fa-5371-11eb-0aa3-e715767d0882
# We can compare strings. We get true since Pluto is later in the dictionary than Jupyter.
"Pluto" > "Jupyter"

# ╔═╡ 72331566-5371-11eb-1c65-a723de42ee84
# Strings are effectively vectors of Chars
s1[1]

# ╔═╡ b0b560a0-5371-11eb-38db-87df842256ba
# But chars are effectively numbers
'H'+1

# ╔═╡ bb889934-5371-11eb-3d22-e13f190c8b1e
# Which can be compared, just like strings
'H' < 'I'

# ╔═╡ 01b4d184-5372-11eb-3f2a-4b925ddc5814
md"## Functions"

# ╔═╡ 055f58ea-5372-11eb-3c24-87169adee76a
# Typical function definition
function f1(x)
	return x^2 + 3
end

# ╔═╡ 10bad430-5372-11eb-3e07-27c21a9bacab
# One-liner-single-expression shorthand
f2(x) = sin(x)-x

# ╔═╡ 22f07c40-5372-11eb-1766-d98ce5c1926a
# Lambda functions. We must use parenthesis if we use multiple arguments.
f3 = (x, y) -> x^x+y^y

# ╔═╡ 3279ba02-5372-11eb-2b99-d5e77b27ab93
md"### Multiple dispatch
The magic feature that makes your life soooo much easier. A function can have several methods, that is, calling different code bodies depending on the input type. This means that most functions in Julia will be just like Python's `print`-function. You can throw almost anything at it, and it will do what you want, as long as there is a sensible definition of the function on the provided domain."

# ╔═╡ 5ae5ffb2-5372-11eb-0e8e-db88a68473f1
function f4(x::Number, s::String)::String
	return string(x) * s
end

# ╔═╡ 78827ec4-5372-11eb-2c57-7917739e670b
function f4(x::T, y::T)::T where T <: Number
	return x*y
end

# ╔═╡ 91ecc82e-5372-11eb-3850-9d0f3d8140a6
# If we throw a number and a string on f4, we'll get a string
f4(3.14, "rad")

# ╔═╡ a69ca9b0-5372-11eb-08c9-c5961e182219
# If we throw two numbers at it, we'll get their product
f4(3, 4)

# ╔═╡ df2afd04-5372-11eb-23d1-b925013291bd
# Note the T in our second definition. That is a type variable. Since we explicitly stated that T has to be a number, but both x and y has to be of type T, we'll get an error of we give it two numbers of different types
try
	f4(3, 4.0)
catch
	"Something went wrong"
end

# ╔═╡ 04bb2a76-5373-11eb-0110-03e15467b6af
md"Initially, this may seem tedious, and note that we don't have to specify any types. The compiler will figure out what to do anyway. What this allows us to do, though, is it allows us to fine-tune our methods for maximum performance, or like the `Plots` library, create a `plot()`-function that will deal with pretty much anything you can throw at it.

### Broadcasting
When running a function on an array, we can use `.`-syntax to broadcast that function across the entire array."

# ╔═╡ d59d084e-5373-11eb-1b40-0fc36fe59f2b
f2.([1,2,3])

# ╔═╡ dfd28a1e-5373-11eb-24ca-f34d1ec6b655
# With lambda functions
(x -> -x).(1:3)

# ╔═╡ Cell order:
# ╟─3aedfbba-536d-11eb-2727-81c5c323f11a
# ╟─85c9e090-536d-11eb-1acd-a34aedb79bd8
# ╠═d78c14c8-5371-11eb-09cc-1f4278ba4954
# ╠═58ba690e-5373-11eb-0532-e5e702f55d95
# ╠═021ccd5a-5377-11eb-3c5f-77b788c97a3b
# ╠═eff86c24-5376-11eb-25f8-b53e4db436ac
# ╠═e116241e-5371-11eb-20b1-b56cb3fdfb92
# ╟─0d1a6cd6-5375-11eb-382e-810bc6bc102b
# ╠═bb60bafa-5375-11eb-2168-6f3c0813321b
# ╠═ab4cc898-5375-11eb-2b32-c3870c66f035
# ╠═11ff0084-5375-11eb-2d7f-7b3ea1d3efe6
# ╠═3cc87d52-5375-11eb-1dad-03a744428e33
# ╠═71f50c04-5375-11eb-16f8-f54418565c4e
# ╟─d11e6a38-5371-11eb-0164-2f1156804f3b
# ╠═c6e26f3e-536d-11eb-208f-6d22b83f5654
# ╠═a939afd6-536d-11eb-1ed0-0b8893733c0a
# ╠═d70a8e6e-536d-11eb-2624-bfeddda6346c
# ╠═00c3029a-536e-11eb-090e-d325ac6925a4
# ╠═368d4fb8-536e-11eb-2618-5912a6ade66f
# ╠═5f0d7c0e-536e-11eb-03e1-9f82a7df928f
# ╠═74ac2182-536e-11eb-0c58-53c6abb75c78
# ╠═902e53da-536e-11eb-1675-1b767226d52e
# ╠═aafedeaa-536e-11eb-3db1-ab41bfcc15ad
# ╠═c3564440-536e-11eb-38e0-e933b6bc2ef7
# ╠═c6e2e24a-536e-11eb-1ff1-3762ce31fb8a
# ╟─d8b07214-536e-11eb-3a49-0fdbfb3b5044
# ╠═e40711e0-536e-11eb-2b1e-5155f1dfc315
# ╠═ed5c72e4-536e-11eb-361c-cbeb82b0c974
# ╠═942cb6ae-5376-11eb-2e31-8d34066d1048
# ╠═8d559e72-5376-11eb-1a75-cdc1bb21f661
# ╠═f704f154-536e-11eb-0231-3d462b18e7f7
# ╠═095dc54a-536f-11eb-3a43-9d7d3cd84c4a
# ╠═926a9732-5374-11eb-0495-652aed838a70
# ╠═309efd88-536f-11eb-2462-f1395e35ab52
# ╠═593a1958-536f-11eb-2f69-8f1a190e80be
# ╠═7be75196-536f-11eb-123c-1772d25157a1
# ╟─85c85956-5376-11eb-10bf-e9e730b8d222
# ╠═8bd68298-536f-11eb-2bd9-7f580489a6f6
# ╠═4a7986c8-5370-11eb-3d8d-4d8b94422721
# ╠═5736f742-5370-11eb-3a87-8d652d05da3d
# ╠═6ab88194-5370-11eb-10c7-a50fc18af76d
# ╠═7e696f20-5370-11eb-05d1-f51d18f56286
# ╠═99a8eb1c-5370-11eb-322a-118b38e8552d
# ╠═9454e03a-5370-11eb-06bf-6d955fe47ecf
# ╠═b38bb3f8-536f-11eb-0315-21350b60b1f3
# ╠═cd4d3e90-5376-11eb-00b4-f33df83df34a
# ╠═3323b3a4-5370-11eb-071f-9bdfd3b105a1
# ╠═3f982142-5370-11eb-08fd-13ab37c4bf91
# ╟─13e4887c-5370-11eb-23bc-693ce7019932
# ╠═1856cd84-5370-11eb-22bb-e50730a81532
# ╠═1d3a10c4-5370-11eb-185c-bde2a6017d47
# ╠═234736cc-5370-11eb-0e6d-2bb0f67cd706
# ╠═2dad88aa-5370-11eb-20a4-19d2855559bf
# ╠═cfc32d84-5370-11eb-0b75-7fad0fea5907
# ╠═eb3b1914-5370-11eb-072d-cd7243ac7508
# ╠═0101e3c2-5371-11eb-0c15-c7d7135cec19
# ╠═1a5016c0-5371-11eb-1a7d-0b8cbe195315
# ╠═2a528290-5371-11eb-221f-49af4e3d2b51
# ╠═423059fa-5371-11eb-0aa3-e715767d0882
# ╠═72331566-5371-11eb-1c65-a723de42ee84
# ╠═b0b560a0-5371-11eb-38db-87df842256ba
# ╠═bb889934-5371-11eb-3d22-e13f190c8b1e
# ╟─01b4d184-5372-11eb-3f2a-4b925ddc5814
# ╠═055f58ea-5372-11eb-3c24-87169adee76a
# ╠═10bad430-5372-11eb-3e07-27c21a9bacab
# ╠═22f07c40-5372-11eb-1766-d98ce5c1926a
# ╟─3279ba02-5372-11eb-2b99-d5e77b27ab93
# ╠═5ae5ffb2-5372-11eb-0e8e-db88a68473f1
# ╠═78827ec4-5372-11eb-2c57-7917739e670b
# ╠═91ecc82e-5372-11eb-3850-9d0f3d8140a6
# ╠═a69ca9b0-5372-11eb-08c9-c5961e182219
# ╠═df2afd04-5372-11eb-23d1-b925013291bd
# ╟─04bb2a76-5373-11eb-0110-03e15467b6af
# ╠═d59d084e-5373-11eb-1b40-0fc36fe59f2b
# ╠═dfd28a1e-5373-11eb-24ca-f34d1ec6b655
